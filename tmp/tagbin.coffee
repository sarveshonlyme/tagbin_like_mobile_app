
# Main Namespace for tagbin functions
T =
	testData: {}
	user: {}
	init: null 			# The initialization function

	notify: null		# Notification module
	picReady: 0
	cI: null
	socket: null
	ninja: document.createElement 'div'

	useOffline: false
	Utils: {}

T.init = ()->
	
	### Check facebook login
	if not FB.getUserID()
		FB.login (_response)->
			console.log "Signed into facebook."
			console.log _response
		, {scope: 'publish_stream'}
	###

	# Full height of .box.app container
	$('.box.app').css('height', (document.height-50).toString() + 'px')

	# T.canvas_sea()

	# Initalize C Sockets
	# T.sockets()

	# Return null
	null

T.handleLoading = ()->

	$(".tb-loader").hide()

	# If .loader is present, set .overlay display: block
	if $(".loading").length then $(".overlay").css('display', 'block')

	$(".tb-loader").show "drop", {direction: "up"}, 500, ()->
		#T.canvasAnim()
		#$(".loading canvas").show()
		null

	window.setTimeout ()->
		$(".tb-loader .anim").removeClass 'anim'
		$(".tb-loader").hide "drop", {direction: 'down'}, 300, ()->
			###$(".loading").hide "fade", ()->
				$(".overlay").css 'display', 'none'
				window.location.href = './pic-upload.html'
			###
			# Show the menu after tagbin animation
			$("ul.loader-menu").show "drop",{direction: "down"}, 400
			$(".tagbin").show "drop", {direction: "down"}, 400
			$(".swipe_notification").show "drop", {direction: "down"}, 400
			$('.tile').show "drop", {direction: "right"}, 1000
	,4000
	null

T.canvasAnim = (_params)->
	#window.clearInterval T.cI
	osfn = _params ? [Math.sin,Math.cos]
	canvas = $(".ldcanvas")[0]
	ctx = canvas.getContext '2d'
	ctx.fillStyle = '#555'
	ctx.strokeStyle = '#555'
	draw = (params)->
		canvas.width = document.width + 2000
		canvas.height = document.height 
		ctx.clearRect 0,0,canvas.width, canvas.height
		x = y = 0
		loop
			xVal = Math.PI/180*(x%360)
			yVal = Math.round ((canvas.height/2) + (Math.sin(xVal)*50)) 

			xVal2 = Math.PI/180*(x%360)
			yVal2 = Math.round ((canvas.height/2) + (Math.cos(xVal)*60)) 

			xVal3 = Math.PI/180*(x%360)
			yVal3 = Math.round ((canvas.height/2) + (Math.sin(xVal+9)*40)) 

			ctx.fillRect(x, yVal, 1,1)
			ctx.fillRect(x, yVal2, 1,1)
			ctx.fillRect(x, yVal3, 1,1)
			#console.log( [xVal,yVal] , [xVal2,yVal2], [xVal3,yVal3] )
			x = x+4
			break if x >= canvas.width
		ctx.save()
		null
	draw(osfn)
	#T.cI = window.setInterval draw(osfn), 3000
	null

T.reset = ()->
	$(".box.main .footer")
		.removeClass("animated")
		.removeClass("needy")
		.text("")
	





T.actions = (_action, _target)->
	switch _action 
	
		when 'navigate' then T.navigate(_target)
		when 'getLikePosts' then T.getLatestPost(_target)
		when 'like_this' then T.like_this(_target)
		else console.log "Unknown Action"

	null 

$(document).ready ()->
	console.log "here"
	$(".loadActions").each (_index)->
		T.actions $(@).attr('data-action'), $(@).attr('data-target')
		null
	null





T.b64toblob = (b64Data, contentType, sliceSize)->
	contentType = contentType || ''
	sliceSize = sliceSize || 1024

	charCodeFromCharacter = (c)->
		return c.charCodeAt(0)

	byteCharacters = atob(b64Data)
	byteArrays = []

	offset = 0

	while offset < byteCharacters.length
		slice = byteCharacters.slice(offset, offset + sliceSize)
		byteNumbers = Array::map.call(slice, charCodeFromCharacter)
		byteArray = new Uint8Array(byteNumbers)
		byteArrays.push byteArray
		offset += sliceSize

	blob = new Blob(byteArrays, {type: contentType})
	return blob;

T.imgToB64 = (url, _callback)->
	cc = document.createElement 'canvas'
	ctx = cc.getContext '2d'
	img = document.createElement 'img'
	img.src = url
	cc.width = img.width 
	cc.height = img.height 
	ctx.drawImage img, 0, 0
	if _callback then _callback(cc.toDataURL())
	else return cc.toDataURL()


T.getXHR = (_url, _type, _callback)->
	if _callback
		type = _type ? "blob"
		xhr = new XMLHttpRequest()
		xhr.open 'GET', _url , true
		xhr.responseType = type
		xhr.onload = (e)->
			#_bloburl = window.webkitURL.createObjectURL this.response
			#_callback(_bloburl)
			_callback this.response
		xhr.send()
	else return console.log "This function requires a callback, Exiting..."
		







T.gdURL = "http://tagbin.in/phpHandler/getDetails.php"

T.latestPostData = null
T.id_to_like = null

T.getLatestPost = (_target)->
	console.log "Fetching latest post"
	_url = "http://tagbin.in/phpHandler/getDetails.php"
	_ops = {type: 'getLatestPost'}
	
	$.post _url, _ops, (response)->
	
		window.RESPONSE1 = JSON.parse(response).data[0]
		T.latestPostData = window.RESPONSE1
		_message = T.latestPostData.message
		console.log _message
		_id = T.latestPostData.from.id
		_from = T.latestPostData.from.name

		$(".username").text _from
		$(".post").text _message
		if T.latestPostData.likes
			_likesCount = T.latestPostData.likes.count | T.latestPostData.likes.data.length
		else
			_likesCount = 0	
		$("#likes_count").text _likesCount.toString()
		$.post T.gdURL, {id: _id, type: 'getUserPicture'}, (_res)->
	
			_bb = T.b64toblob _res, 'image/jpg'
			_bburl = window.webkitURL.createObjectURL _bb
			$('#userpic img').attr 'src', _bburl
			T.mobile()

		null
	null

T.mobile = ()->
	T.serial.readM (_tagid)->
		console.log _tagid
		$(T.ninja).trigger 'likePost'
		window.setTimeout ()->
			T.mobile()
		,5000










$(T.ninja).on 'likePost', (e)->
	_url = "http://tagbin.in/phpHandler/fb_likepost.php"
	_options = 
		postID: T.latestPostData.id
		tagID: T.serial.tagID
	$.post _url, _options, (_response)->
		console.log _response
		if _response.slice(_response.length-5, _response.length) != 'likes'
			console.log "You're tagID is not registered or expired"
		else
			if _options.postID in T.likePosts.liked
				console.log "You've already liked"
			else
				T.likePosts.liked.push _options.postID
				console.log 'Liked...'
				T.getLatestPost()
				T.getPoint 'like', T.serial.tagID
		null
	null

T.like_this = ()->
	$(T.ninja).trigger 'likePost'

$(document).ready ()->
	window.setInterval ()->
		console.log "afahuifh"
		T.getLatestPost()
	,5000





T.serial =
	DEVICE_READY: false
	DEVICE_TIMEOUT: 500
	DEVICE_PORT: "/dev/ttyACM0"
	DEVICE_VENDORID: '2341'
	DEVICE_PRODUCTD: '0010'

	# Short Namespace for easy code
	S: chrome.serial

	interval:  null
	connInfo: null
	connId: null
	tagID: null

	isProcessing: false

	callback: null

	readM: (_cb)->
		if T.serial.isProcessing then window.clearInterval(T.serial.interval)
		T.serial.isProcessing = true
		T.serial.open()
		T.serial.onRead()
		T.serial.callback = _cb
		null
	
	open: ()->
		###T.serial.S.getPorts (ports)->
			T.DEVICE_PORT = ports[0]
			console.log T.DEVICE_PORT, "Proceed to reading tag"
		###
		S = chrome.serial
		S.open T.serial.DEVICE_PORT, {bitrate: 9600}, T.serial.onOpen
		null

	onOpen: (connectionInfo)->
		T.serial.connInfo = connectionInfo
		T.serial.connId = T.serial.connInfo.connectionId
		if T.serial.connInfo.connectionId is -1 then console.log "System Permissions error on: "+ T.serial.DEVICE_PORT
		if typeof T.serial.connInfo.connectionId is 'number' then T.serial.listen()
		else console.error "Port cannot be opened, Some other error has occured"

	listen: ()->
		setInterval = setInterval || window.setInterval
		T.serial.interval = setInterval ()->
			T.serial.S.read T.serial.connId, 128, T.serial.onRead
		, T.serial.DEVICE_TIMEOUT
		null

	onRead: (readInfo)->
		if not T.serial.connId then return
		if readInfo and readInfo.bytesRead > 0 and readInfo.data then T.serial.processOutput readInfo.data
		else console.log "Swipe tag to read"

	processOutput: (_arrayBuffer)->
		console.log "here"
		str = T.serial._ab2str _arrayBuffer
		# Filter out the tag from junk data
		if str.length == 13
			T.serial.tagID = str
			T.serial.readSuccess()
			return
		str = str.split('\n')[0].split(' ')[0]
		
		if str is '' or NaN or " " or "\n"
			console.log "Retrying..."
			$(T.ninja).trigger 'showRetry'
		
		if str.length.toString() is '22' or str.length.toString() is '24'
			T.serial.tagID = str
			T.serial.readSuccess()
		
		else 
			doNothing = 1
	
	readSuccess: ()->
		
		# Clear the repeating interval
		clearInterval = clearInterval || window.clearInterval
		clearInterval T.serial.interval

		# Close the serial port 	#!IMPORTANT
		T.serial.S.close T.serial.connId, (result)->
			console.log "Serial Port Closed"
			console.log "Value of tag from T.serial.tagID"
			T.serial.isProcessing = false
		# Return Value to Callback
		if typeof(T.serial.callback) is 'function' then T.serial.callback(T.serial.tagID)
		else console.log "No Callbacks, use T.readM(_callback) for reading tag with a callback"

	readTag: (readInfo)->
		str = T.serial._ab2str readInfo.data
		console.log "Tag read successful, tagID: " + str.toString()
		console.log T.serial.tagID + str
		tmp = T.serial.tagID + str
		T.serial.tagID = tmp.split('\n')[0].split(' ')[0]
		$(T.ninja).trigger 'gotTagID'
		T.serial.reset()
		null

	fullTagID: (readInfo)->
		if not T.serial.connId then return 
		if readInfo and readInfo.bytesRead > 0 and readInfo.data
			_data = T.serial._ab2str readInfo.data
			T.serial.tagID = _data.split('\n')[0].split(' ')[0]
			console.log "tagID: "+ T.serial.tagID
			$(T.ninja).trigger 'gotTagID'
		else
			console.log "Swipe tag to read"

	reset: ()->
		clearInterval = clearInterval || window.clearInterval
		clearInterval T.serial.interval
		T.serial.S.close T.serial.connId, (result)->
			console.log "Closed Serial Port"
		T.serial.open()
		null

	_ab2str: (buf)->
		return String.fromCharCode.apply null, new Uint8Array(buf)

T.checkTag = (_cbb)->
	T.serial.readM ()->
		$.post 'http://tagbin.in/phpHandler/getDetails.php', {type:'getUserName', tagID: T.serial.tagID}, (res)->
			a = JSON.parse res
			$('.greeting').text "Hello, "+a.firstname + " " + a.lastname
			null
		$.post "http://tagbin.in/phpHandler/getDetails.php", {tagID: T.serial.tagID, type: 'getUserDetails'}, (_res)->
			a = JSON.parse _res
			T.getXHR "http://graph.facebook.com/" + a.fbuserid + "/picture?type=large", "blob", (responseBlob)->
				link = window.webkitURL.createObjectURL responseBlob
				$('.userpic').attr 'src', link	#	css 'background-image', 'url("' + link + '")'
				$('#image').hide "drop", {direction:'left'}, 500, ()->
					$('#userpic').show "drop", {direction:'right'}, 500
					window.setTimeout ()->
						$('#userpic').hide "drop", {direction:'left'}, 500, ()->
							$('#image').show "drop", {direction:'right'}, 500
					,4000
					null
				null
		_cbb()
		T.checkTag (_tagid)->
			console.log T.serial.tagID
			null
		null
	null




